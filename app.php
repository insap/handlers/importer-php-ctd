<?php

use App\Importer;
use App\Illuminate\Storage;
use App\Illuminate\StorageParams;

require __DIR__ . '/vendor/autoload.php';

$options = getopt('', [ 'test::']);
$test = $options['test'] ?? null;

if ($test) {
    // OK
    die(0);
}

# Директория с данными из сервиса
$dataPath = __DIR__ . '/data';
# Директория с файлами из сервиса
$filesPath = __DIR__ . '/data/files';

# Директории куда импортер будет складывать результат своей работы
$resultPath = __DIR__ . '/result/';
# Директория с файлами из сервиса
$resultFilesPath = __DIR__ . '/result/files';

# Очищаем директории перед тем как их заполнять, и заново создаем их пустыми
Storage::deleteAllFilesFromDir($resultPath, true);
Storage::deleteAllFilesFromDir($resultFilesPath, true);

# Получаем данные которые пришли в импортер из сервиса
# params - данные из формы, которую заполнял юзер для импорта
$params = Storage::readJson($dataPath . '/params.json');
# files - это файл с данными о расположении файлов, которые юзер загружал для импорта
$files = Storage::readJson($dataPath . '/files.json');
# data - данные с БД сервиса
$data = Storage::readJson($dataPath . '/data.json');

$storageParams = new StorageParams($params, $files, $data, $filesPath);

$data = (new Importer($storageParams))->import();

# Записываем полученные данные в файл с данными
Storage::writeJson( $resultPath . '/' . 'data.json', $data);

# Записываем полученные параметры из сервиса
Storage::writeJson( $resultPath . '/' . 'params.json', $params);

# Данный импортер не возвращает никаких файлов, а только возвращает обработанные данные
Storage::writeJson( $resultPath . '/' . 'files.json', $files);

(new \App\Illuminate\FileCopier($filesPath, $resultFilesPath))->copyFiles();

die(0);

