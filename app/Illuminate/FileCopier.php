<?php

declare(strict_types=1);

namespace App\Illuminate;

class FileCopier {
    public function __construct(
        private string $sourceDir,
        private string $destinationDir
    ) {}

    public function copyFiles(): void {
        if (!is_dir($this->sourceDir) || !is_dir($this->destinationDir)) {
            die("Исходная или целевая директория не существует.");
        }

        $files = scandir($this->sourceDir, SCANDIR_SORT_NONE);

        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                $sourceFile = $this->sourceDir . '/' . $file;
                $destinationFile = $this->destinationDir . '/' . $file;

                if (is_file($sourceFile)) {
                    copy($sourceFile, $destinationFile);
                }
            }
        }
    }
}