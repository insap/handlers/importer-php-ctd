<?php

namespace App\Illuminate;

use App\DTO\FileDto;
use Illuminate\Support\Collection;

class StorageParams
{
    public Collection $params;
    public Collection $files;
    public Collection $data;
    private string $filesPath;

    public function __construct( array $params, array $files, array $data, string $filesPath)
    {
        $this->params = collect($params);
        $this->files = collect($files)->map(fn(array $file) => FileDto::fromArray($file));
        $this->data = collect($data);

        $this->filesPath = $filesPath;
    }

    public function getFilePathByAlias(string $alias): string
    {
        return $this->filesPath . DIRECTORY_SEPARATOR . $this->files->first(fn(FileDto $dto) => $dto->alias === $alias)->hashName;
    }
}