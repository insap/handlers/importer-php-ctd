<?php

namespace App;

use App\Illuminate\StorageParams;
use Carbon\Carbon;

class Importer
{
    protected StorageParams $storageParams;

    public function __construct(StorageParams $storageParams)
    {
        $this->storageParams = $storageParams;
    }

    public function import(): array
    {
        $file = $this->storageParams->getFilePathByAlias('data');

        $lines = (array)\file($file);
        $result = [];

        $dateTime = $this->storageParams->params->get('date_time');

        $headers = false;
        $isDagestan2021 = false;
        $columns = [];
        foreach ($lines as $line) {
            if ($headers === false) {
                $headers = true;
                $isDagestan2021 = str_contains($line, "ntu_average");

                if ($isDagestan2021) {
                    $columns = collect(explode(' ', $line));

                    // Убираем столбцы ntu
                    if ($columns->first(fn(string $column) => str_contains($column, 'ntu_average'))) {
                        $columns = $columns->filter(
                            fn(string $column) => !str_contains($column, 'ntu_average') && !str_contains($column, 'ntu_1')  && !str_contains($column, 'ntu_2')
                        );
                    }
                    continue;
                }
            }

            if ($isDagestan2021) {
                $data = explode(' ', $line);
                $item = array_combine($columns->all(), array_slice($data, 0, count($columns)));

                array_walk_recursive($item, fn(&$item) => $item = $this->sanitize($item));

                $result[] = [
                    'date' => Carbon::parse($dateTime)->format('Y-m-d 00:00:00'),
                    'temperature' => (float)str_replace(',', '.', $item['temperature']),
                    'turbidity' => (float)str_replace(',', '.', $item['turbidity']),
                    'salinity' => (float)str_replace(',', '.', $item['salinity']),
                    'depth' => (float)str_replace(',', '.', $item['depth']),
                    'chlorophyll_a' => (float)str_replace(',', '.', $item['chlorophyll_a']),
                    'speed_of_sound' => (float)str_replace(',', '.', $item['speed_of_sound']),
                    'depth2' => (float)str_replace(',', '.', $item['depth2']),

                    'step_id' => (int)$item['step_id'],
                    'station' => (int)$item['station'],

                    'latitude' => (float)$item['latitude'],
                    'longitude' => (float)$item['longitude'],
                ];
                continue;
            }

            $split = str_contains($line, ',,') ? explode(',,', $line) : explode(';', $line);
            if ($this->isDagestan2018($split[0])) {
                $result[] = [
                    'date' => Carbon::createFromFormat('m/d/Y H:i:s', $split[0] . " " . $split[1])->format('Y-m-d H:i:s'),
                    'temperature' => (float)str_replace(',', '.', $split[6]),
                    'turbidity' => (float)str_replace(',', '.', $split[5]),
                    'salinity' => (float)str_replace(',', '.', $split[7]),
                    'depth' => (float)str_replace(',', '.', $split[8]),

                    'step_id' => (int)$split[10],
                    'station' => (int)$split[2],

                    'latitude' => (float)str_replace(',', '.', $split[3]),
                    'longitude' => (float)str_replace(',', '.', $split[4]),
                ];
            } else if ($this->isDagestan2022($split[0])) {
                $result[] = [
                    'date' => Carbon::parse($dateTime)->format('Y-m-d 00:00:00'),
                    'temperature' => (float)str_replace(',', '.', $split[4]),
                    'turbidity' => (float)str_replace(',', '.', $split[5]),
                    'salinity' => (float)str_replace(',', '.', $split[8]),
                    'depth' => (float)str_replace(',', '.', $split[7]),
                    'chlorophyll_a' => (float)str_replace(',', '.', $split[6]),
                    'speed_of_sound' => (float)str_replace(',', '.', $split[9]),
                    'depth2' => (float)str_replace(',', '.', $split[10]),

                    'ntu_1' => (float)str_replace(',', '.', $split[11]),
                    'ntu_2' => (float)str_replace(',', '.', $split[12]),
                    'ntu_average' => (float)str_replace(',', '.', $split[13]),

                    'step_id' => (int)$split[2],
                    'station' => (int)$split[3],

                    'latitude' => (float)str_replace(',', '.', $split[0]),
                    'longitude' => (float)str_replace(',', '.', $split[1]),
                ];
            }


        }

        return $result;
    }

    private function isDagestan2018(string $split): bool
    {
        return str_contains($split, '/');
    }

    private function isDagestan2022(string $split): bool
    {
        return str_contains($split, ',');
    }

    private function sanitize(string $str): string
    {
        $preprocessedString = str_replace(["\r\n", "\r", "\n"], ' ' ,$str);
        $preprocessedString = preg_replace('/\s+/', ' ', $preprocessedString);

        return trim($preprocessedString);
    }
}