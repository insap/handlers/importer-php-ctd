<?php

declare(strict_types=1);

namespace App\DTO;

readonly class FileDto
{
    public function __construct(
        public string $hashName,
        public string $original,
        public string $extension,
        public string $mime,
        public string $alias,
    )
    {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->hashName,
            'original' => $this->original,
            'extension' => $this->extension,
            'mime' => $this->mime,
            'type' => $this->alias,
            'alias' => $this->alias,
        ];
    }

    public static function fromArray(array $array): self
    {
        return new self(
            hashName: $array['name'],
            original: $array['original'],
            extension: $array['extension'],
            mime: $array['mime'],
            alias: $array['alias']
        );
    }
}